package com.example.kira.capstoneproject.ListImageAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kira.capstoneproject.Object.ImageObject;
import com.example.kira.capstoneproject.R;

import java.util.List;

/**
 * Created by KIRA on 12/3/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

    private List<ImageObject> itemList;
    private Context context;

    public RecyclerViewAdapter(Context context, List<ImageObject> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_list, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.imgItem.setImageDrawable(itemList.get(position).getImageItem());
    }

    @Override
    public int getItemCount() {
        return itemList.size() != 0 || itemList == null ? itemList.size() : 0;
    }
}
