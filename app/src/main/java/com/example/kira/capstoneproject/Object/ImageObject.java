package com.example.kira.capstoneproject.Object;

import android.graphics.drawable.Drawable;

/**
 * Created by KIRA on 12/3/2017.
 */

public class ImageObject {

    private Drawable imageItem;

    public Drawable getImageItem() {
        return imageItem;
    }

    public void setImageItem(Drawable imageItem) {
        this.imageItem = imageItem;
    }

    public ImageObject(Drawable imageItem) {
        this.imageItem = imageItem;
    }

    public ImageObject() {
    }
}
