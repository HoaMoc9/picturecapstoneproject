package com.example.kira.capstoneproject.Utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

/**
 * Created by KIRA on 12/3/2017.
 */

public class FaceOverlayView extends android.support.v7.widget.AppCompatImageView {
    private static final float NUMBER_SCALE = 50;
    private Bitmap mBitmap;
    private SparseArray<Face> mFaces;
    private float centerX, centerY, radius, left, top, right, bottom;
    private Paint paint;
    private SetScaleImageView mSetScaleImageView;

    public void setmSetScaleImageView(SetScaleImageView mSetScaleImageView) {
        this.mSetScaleImageView = mSetScaleImageView;
    }

    public Point getPoint() {
        return new Point((int) centerX, (int) centerY);
    }

    public FaceOverlayView(Context context) {
        this(context, null);
        initView();

    }


    public FaceOverlayView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        initView();
    }

    public FaceOverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
        FaceDetector detector = new FaceDetector.Builder(getContext())
                .setTrackingEnabled(true)
                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                .setMode(FaceDetector.ACCURATE_MODE)
                .build();

        if (!detector.isOperational()) {
            //Handle contingency
        } else {
            Frame frame = new Frame.Builder().setBitmap(bitmap).build();
            mFaces = detector.detect(frame);
            detector.release();

        }
        invalidate();
    }

    private void initView() {
        //emphasis.
        paint = new Paint();
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);

        radius = 100;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();
        if ((mBitmap != null) && (mFaces != null)) {
            double scale = drawBitmap(canvas);
            drawFaceBox(canvas, scale);

        }
        canvas.restore();
    }

//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//
//        switch (event.getAction()) {
//
//            case MotionEvent.ACTION_DOWN:
//                startX = event.getX();
//                startY = event.getY();
//
//                break;
//            case MotionEvent.ACTION_MOVE:
//                float xMove = event.getX();
//                float yMove = event.getY();
//                if (checkTouchCircle(xMove, yMove)) {
//                    float dx = Math.abs(centerX - xMove);
//                    float dy = Math.abs(centerY - yMove);
//                    float radiusTouch = getRadiusTouch(dx, dy);
//                    if (radiusTouch >= 100) {
//                        radius = radiusTouch;
//                        Log.e("radiuschange", "" + radius);
//                        invalidate();
//                    } else {
//                        radius = 100;
//                        Log.e("radiuschange", "" + radius);
//
//                    }
//
//                }
//
//
//                break;
//            case MotionEvent.ACTION_UP:
//                break;
//        }
//        return true;
//    }

//
//    private float getRadiusTouch(float dx, float dy) {
//        return (float) Math.sqrt(dx * dx + dy * dy);
//    }
//
//    private boolean checkTouchCircle(float xTouch, float yTouch) {
//        float dx = Math.abs(centerX - xTouch);
//        float dy = Math.abs(centerY - yTouch);
//        float radiusTouch = getRadiusTouch(dx, dy);
//        float dRadius = Math.abs(radius - radiusTouch);
//        if (dRadius <= NUMBER_SCALE) {
//            return true;
//        }
//        return false;
//    }

    private double drawBitmap(Canvas canvas) {
        double viewWidth = canvas.getWidth();
        double viewHeight = canvas.getHeight();
        double imageWidth = mBitmap.getWidth();
        double imageHeight = mBitmap.getHeight();
        double scale = Math.min(viewWidth / imageWidth, viewHeight / imageHeight);
        Log.e("FACEOVERLAYVIEW", "" + scale);

        Rect destBounds = new Rect(0, 0, (int) (imageWidth * scale), (int) (imageHeight * scale));
        canvas.drawBitmap(mBitmap, null, destBounds, null);
        return scale;
    }

    private void drawFaceBox(Canvas canvas, double scale) {

//        float left = 0;
//        float top = 0;
//        float right = 0;
//        float bottom = 0;

        for (int i = 0; i < mFaces.size(); i++) {
            Face face = mFaces.valueAt(i);

            left = (float) (face.getPosition().x * scale);
            top = (float) (face.getPosition().y * scale);
            right = (float) scale * (face.getPosition().x + face.getWidth());
            bottom = (float) scale * (face.getPosition().y + face.getHeight());

            centerX = left + (right - left) / 2;
            centerY = top + 2 * (bottom - top) / 3;

            canvas.drawRect(left, top, right, bottom, paint);
            mSetScaleImageView.getImageView(right - left, bottom - top);


        }
    }
//
//    private void drawFaceLandmarks(Canvas canvas, double scale) {
//        for (int i = 0; i < mFaces.size(); i++) {
//            Face face = mFaces.valueAt(i);
//
//            for (Landmark landmark : face.getLandmarks()) {
//                int cx = (int) (landmark.getPosition().x * scale);
//                int cy = (int) (landmark.getPosition().y * scale);
//                switch (landmark.getType()) {
//                    case Landmark.LEFT_EYE:
//
//                        canvas.drawCircle(cx, cy, 20, paint);
//
//                        break;
//                    case Landmark.RIGHT_EYE:
//
//                        canvas.drawCircle(cx, cy, 20, paint);
//
//                        break;
//                    case Landmark.BOTTOM_MOUTH:
//
//                        canvas.drawCircle(cx, (cy - (int) scale), 20, paint);
//
//                        break;
//                }
//            }
//
//        }
//    }

    public interface SetScaleImageView {
        void getImageView(float left, float top);
    }

}
