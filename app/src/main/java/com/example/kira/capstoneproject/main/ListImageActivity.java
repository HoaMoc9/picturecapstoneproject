package com.example.kira.capstoneproject.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kira.capstoneproject.ListImageAdapter.RecyclerViewAdapter;
import com.example.kira.capstoneproject.Object.ImageObject;
import com.example.kira.capstoneproject.R;
import com.example.kira.capstoneproject.SavedImageList.SavedImageList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HangPC on 12/3/2017.
 */

public class ListImageActivity extends AppCompatActivity{

    private GridLayoutManager lLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_image);
        initToolbar();


        List<ImageObject> rowListItem = getAllItemList();
        lLayout = new GridLayoutManager(ListImageActivity.this, 2);

        RecyclerView rView = (RecyclerView) findViewById(R.id.recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        RecyclerViewAdapter rcAdapter = new RecyclerViewAdapter(ListImageActivity.this, rowListItem);
        rView.setAdapter(rcAdapter);
    }

    private void initToolbar() {

        Toolbar topToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView titleToolbar = (TextView) this.findViewById(R.id.toolbar_title);
        ImageView btnBack = (ImageView) this.findViewById(R.id.toolbar_back);

        titleToolbar.setText(getString(R.string.activty_list_image_title));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, AppMainActivity.class);
        startActivity(intent);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
////        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

    public List<ImageObject> getAllItemList() {
        List<ImageObject> allItems = new ArrayList<ImageObject>();
        Drawable drawableItem;
        for (Bitmap imageBitmap : SavedImageList.savedImagedList) {
            drawableItem = new BitmapDrawable(imageBitmap);
            allItems.add(new ImageObject(drawableItem));
        }

        return allItems;
    }
}
